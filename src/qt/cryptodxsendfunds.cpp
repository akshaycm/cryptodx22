// Copyright (c) 2018 The CryptoDX Developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#include "cryptodxsendfunds.h"
#include "cryptodxsendfundsrequest.h"

#include "optionsmodel.h"

#include <QTimer>
#include <QEvent>
#include <QMessageBox>
#include <QList>
#include <QDebug>

enum BSendFundsCrumbs {
    RECIPIENT = 1,
    AMOUNT,
    TRANSACTION_FEE,
    REVIEW_PAYMENT,
    DONE,
};

CryptoDXSendFunds::CryptoDXSendFunds(WalletModel *w, QFrame *parent) : QFrame(parent), walletModel(w),
                                                                       layout(new QVBoxLayout),
                                                                       model(new CryptoDXSendFundsModel) {
//    this->setStyleSheet("border: 1px solid red");
    this->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    layout->setContentsMargins(QMargins());
    this->setLayout(layout);

    page1 = new CryptoDXSendFunds1(walletModel, RECIPIENT);
    page2 = new CryptoDXSendFunds2(walletModel, AMOUNT);
    page3 = new CryptoDXSendFunds3(walletModel, TRANSACTION_FEE);
    page4 = new CryptoDXSendFunds4(walletModel, REVIEW_PAYMENT);
    pages = { page1, page2, page3, page4 };

    done = new CryptoDXSendFundsDone;
    done->hide();

    breadCrumb = new CryptoDXBreadCrumb;
    breadCrumb->setParent(this);
    breadCrumb->addCrumb(tr("Recipient"), RECIPIENT);
    breadCrumb->addCrumb(tr("Amount"), AMOUNT);
    breadCrumb->addCrumb(tr("Transaction Fee"), TRANSACTION_FEE);
    breadCrumb->addCrumb(tr("Review Payment"), REVIEW_PAYMENT);
    breadCrumb->show();

    connect(breadCrumb, SIGNAL(crumbChanged(int)), this, SLOT(crumbChanged(int)));
    connect(page1, SIGNAL(next(int)), this, SLOT(nextCrumb(int)));
    connect(page2, SIGNAL(next(int)), this, SLOT(nextCrumb(int)));
    connect(page3, SIGNAL(next(int)), this, SLOT(nextCrumb(int)));
    connect(page2, SIGNAL(back(int)), this, SLOT(prevCrumb(int)));
    connect(page3, SIGNAL(back(int)), this, SLOT(prevCrumb(int)));
    connect(page4, SIGNAL(back(int)), this, SLOT(prevCrumb(int)));
    connect(page1, SIGNAL(cancel(int)), this, SLOT(onCancel(int)));
    connect(page2, SIGNAL(cancel(int)), this, SLOT(onCancel(int)));
    connect(page3, SIGNAL(cancel(int)), this, SLOT(onCancel(int)));
    connect(page4, SIGNAL(cancel(int)), this, SLOT(onCancel(int)));
    connect(page4, SIGNAL(edit()), this, SLOT(onEdit()));
    connect(page4, SIGNAL(submit()), this, SLOT(onSendFunds()));
    connect(done, SIGNAL(dashboard()), this, SLOT(onDoneDashboard()));
    connect(done, SIGNAL(payment()), this, SLOT(reset()));

    // Estimated position
    positionCrumb(QPoint(175, -4));
    breadCrumb->goToCrumb(RECIPIENT);
}

void CryptoDXSendFunds::focusInEvent(QFocusEvent *event) {
    QWidget::focusInEvent(event);
    if (screen)
        screen->setFocus();
}

void CryptoDXSendFunds::crumbChanged(int crumb) {
    if (screen && crumb > breadCrumb->getCrumb() && breadCrumb->showCrumb(breadCrumb->getCrumb()) && !screen->validated())
        return;
    breadCrumb->showCrumb(crumb);

    if (screen) {
        layout->removeWidget(screen);
        screen->hide();
    }

    if (!done->isHidden()) {
        layout->removeWidget(done);
        done->hide();
    }

    switch(crumb) {
        case RECIPIENT:
            screen = page1;
            break;
        case AMOUNT:
            screen = page2;
            break;
        case TRANSACTION_FEE:
            screen = page3;
            break;
        case REVIEW_PAYMENT:
            screen = page4;
            break;
        default:
            return;
    }
    screen->setData(model);
    layout->addWidget(screen);

    positionCrumb();
    screen->show();
    screen->setFocus();
}

void CryptoDXSendFunds::nextCrumb(int crumb) {
    if (screen && crumb > breadCrumb->getCrumb() && breadCrumb->showCrumb(breadCrumb->getCrumb()) && !screen->validated())
        return;
    if (crumb >= REVIEW_PAYMENT) // do nothing if at the end
        return;
    breadCrumb->goToCrumb(++crumb);
}

void CryptoDXSendFunds::prevCrumb(int crumb) {
    if (!screen)
        return;
    if (crumb <= RECIPIENT) // do nothing if at the beginning
        return;
    breadCrumb->goToCrumb(--crumb);
}

void CryptoDXSendFunds::onCancel(int crumb) {
    clear();
    breadCrumb->goToCrumb(RECIPIENT);
    emit dashboard();
}

void CryptoDXSendFunds::onEdit() {
    breadCrumb->goToCrumb(AMOUNT);
}

void CryptoDXSendFunds::onSendFunds() {
    clear();
    goToDone();
}

void CryptoDXSendFunds::onDoneDashboard() {
    model->reset();
    for (CryptoDXSendFundsPage *page : pages)
        page->clear();
    breadCrumb->goToCrumb(RECIPIENT);
    emit dashboard();
}

bool CryptoDXSendFunds::event(QEvent *event) {
    if (screen && event->type() == QEvent::LayoutRequest) {
        positionCrumb();
    } else if (event->type() == QEvent::Type::MouseButtonPress) {
        auto *w = this->focusWidget();
        if (dynamic_cast<const QLineEdit*>(w) != nullptr && w->hasFocus() && !w->underMouse())
            w->clearFocus();
    }
    return QFrame::event(event);
}

void CryptoDXSendFunds::reset() {
    model->reset();
    for (CryptoDXSendFundsPage *page : pages)
        page->clear();
    breadCrumb->goToCrumb(RECIPIENT);
}

void CryptoDXSendFunds::positionCrumb(QPoint pt) {
    if (pt != QPoint() || pt.x() > 250 || pt.y() > 0) {
        breadCrumb->move(pt);
        breadCrumb->raise();
        return;
    }
    auto *pageHeading = screen->findChild<QWidget*>("h4", Qt::FindDirectChildrenOnly);
    QPoint npt = pageHeading->mapToGlobal(QPoint(pageHeading->width(), 0));
    npt = this->mapFromGlobal(npt);
    breadCrumb->move(npt.x() + 20, npt.y() + pageHeading->height() - breadCrumb->height() - 2);
    breadCrumb->raise();
}

void CryptoDXSendFunds::goToDone() {
    layout->removeWidget(screen);
    screen->hide();
    layout->addWidget(done);
    done->show();
}

