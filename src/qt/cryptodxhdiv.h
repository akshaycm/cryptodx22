// Copyright (c) 2018 The CryptoDX Developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#ifndef BLOCKNETHDIV_H
#define BLOCKNETHDIV_H

#include <QLabel>

class CryptoDXHDiv : public QLabel
{
    Q_OBJECT
public:
    explicit CryptoDXHDiv(QLabel *parent = nullptr);

signals:

public slots:
};

#endif // BLOCKNETHDIV_H
