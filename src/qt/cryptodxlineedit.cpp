// Copyright (c) 2018 The CryptoDX Developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#include <QEvent>
#include "cryptodxlineedit.h"

CryptoDXLineEdit::CryptoDXLineEdit(int w, int h, QLineEdit *parent) : QLineEdit(parent) {
    this->setMinimumSize(w, h);
}

void CryptoDXLineEdit::setID(const QString id) {
    this->id = id;
}

QString CryptoDXLineEdit::getID() {
    return this->id;
}

