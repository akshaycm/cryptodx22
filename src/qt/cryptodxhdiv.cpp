// Copyright (c) 2018 The CryptoDX Developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#include "cryptodxhdiv.h"

CryptoDXHDiv::CryptoDXHDiv(QLabel *parent) : QLabel(parent) {
    this->setObjectName("hdiv");
    this->setFixedHeight(1);
    this->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
}
