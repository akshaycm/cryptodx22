// Copyright (c) 2018 The CryptoDX Developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#ifndef BLOCKNETSENDFUNDS1_H
#define BLOCKNETSENDFUNDS1_H

#include "cryptodxsendfundsutil.h"
#include "cryptodxformbtn.h"
#include "cryptodxaddresseditor.h"

#include "walletmodel.h"

#include <QFrame>
#include <QVBoxLayout>
#include <QLabel>

class CryptoDXSendFunds1 : public CryptoDXSendFundsPage {
    Q_OBJECT
protected:
    void keyPressEvent(QKeyEvent *event) override;
    void focusInEvent(QFocusEvent *event) override;

public:
    explicit CryptoDXSendFunds1(WalletModel *w, int id, QFrame *parent = nullptr);
    void setData(CryptoDXSendFundsModel *model) override;
    bool validated() override;

public slots:
    void clear() override;
    void textChanged();
    void onAddressesChanged();

private:
    QVBoxLayout *layout;
    QLabel *titleLbl;
    CryptoDXAddressEditor *addressTi;
    CryptoDXFormBtn *continueBtn;
};

#endif // BLOCKNETSENDFUNDS1_H
