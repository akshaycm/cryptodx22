// Copyright (c) 2018 The CryptoDX Developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#ifndef BLOCKNETSENDFUNDS3_H
#define BLOCKNETSENDFUNDS3_H

#include "cryptodxsendfundsutil.h"
#include "cryptodxformbtn.h"
#include "cryptodxlineedit.h"

#include "walletmodel.h"
#include "coincontrol.h"

#include <QFrame>
#include <QVBoxLayout>
#include <QLabel>
#include <QRadioButton>
#include <QShowEvent>
#include <QHideEvent>

class CryptoDXSendFunds3 : public CryptoDXSendFundsPage {
    Q_OBJECT
protected:
    void keyPressEvent(QKeyEvent *event) override;
    void showEvent(QShowEvent *event) override;
    void hideEvent(QHideEvent *event) override;

public:
    explicit CryptoDXSendFunds3(WalletModel *w, int id, QFrame *parent = nullptr);
    void setData(CryptoDXSendFundsModel *model) override;
    bool validated() override;
    void clear() override;

signals:
    void feeOption(double fee);

private slots:
    void onFeeDesignation();
    void onSpecificFee(const QString& = QString());
    void onEncryptionStatus(int encStatus);

private:
    int displayUnit;

    QVBoxLayout *layout;
    QLabel *titleLbl;
    QLabel *recommendedDescLbl;
    QRadioButton *recommendedRb;
    QRadioButton *specificRb;
    CryptoDXLineEdit *specificFeeTi;
    QLabel *specificFeeLbl;
    QLabel *totalFeeLbl;
    CryptoDXFormBtn *continueBtn;
    CryptoDXFormBtn *cancelBtn;
    QLabel *transactionFeeDesc;

    void updateFee();
    void updateTxFees(CAmount fee);
    void showZeroFeeMsg(bool show = true);
};

#endif // BLOCKNETSENDFUNDS3_H
