// Copyright (c) 2018 The CryptoDX Developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#include "cryptodxsendfunds1.h"

#include <QMessageBox>
#include <QKeyEvent>

CryptoDXSendFunds1::CryptoDXSendFunds1(WalletModel *w, int id, QFrame *parent) : CryptoDXSendFundsPage(w, id, parent),
                                                                                 layout(new QVBoxLayout) {
//    this->setStyleSheet("border: 1px solid red");
    this->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    this->setLayout(layout);
    layout->setContentsMargins(15, 10, 35, 30);

    titleLbl = new QLabel(tr("Send Funds"));
    titleLbl->setObjectName("h4");

    QLabel *subtitleLbl = new QLabel(tr("Who would you like to send funds to?"));
    subtitleLbl->setObjectName("h2");

    addressTi = new CryptoDXAddressEditor(675);
    addressTi->setPlaceholderText(tr("Enter CryptoDX Address..."));

    continueBtn = new CryptoDXFormBtn;
    continueBtn->setText(tr("Continue"));
    continueBtn->setDisabled(true);

    QLabel *hdiv = new QLabel;
    hdiv->setFixedHeight(1);
    hdiv->setObjectName("hdiv");

    layout->addWidget(titleLbl, 0, Qt::AlignTop | Qt::AlignLeft);
    layout->addSpacing(45);
    layout->addWidget(subtitleLbl, 0, Qt::AlignTop);
    layout->addSpacing(25);
    layout->addWidget(addressTi);
    layout->addSpacing(45);
    layout->addWidget(hdiv);
    layout->addSpacing(45);
    layout->addWidget(continueBtn);
    layout->addStretch(1);

    connect(addressTi, SIGNAL(textChanged()), this, SLOT(textChanged()));
    connect(addressTi, SIGNAL(addresses()), this, SLOT(onAddressesChanged()));
    connect(addressTi, SIGNAL(returnPressed()), this, SLOT(onNext()));
    connect(continueBtn, SIGNAL(clicked()), this, SLOT(onNext()));
}

void CryptoDXSendFunds1::setData(CryptoDXSendFundsModel *model) {
    CryptoDXSendFundsPage::setData(model);
}

void CryptoDXSendFunds1::clear() {
    addressTi->clear();
}

void CryptoDXSendFunds1::focusInEvent(QFocusEvent *event) {
    QWidget::focusInEvent(event);
    addressTi->setFocus();
}

void CryptoDXSendFunds1::keyPressEvent(QKeyEvent *event) {
    QWidget::keyPressEvent(event);
    if (this->isHidden())
        return;
    if (event->key() == Qt::Key_Enter || event->key() == Qt::Key_Return)
        onNext();
}

void CryptoDXSendFunds1::textChanged() {
    continueBtn->setDisabled(addressTi->getAddresses().isEmpty());
}

bool CryptoDXSendFunds1::validated() {
    if (addressTi->getAddresses().isEmpty()) {
        QMessageBox::warning(this->parentWidget(), tr("Issue"), tr("Please add a valid address to the send box."));
        return false;
    }

    // Use wallet to validate address
    auto addresses = addressTi->getAddresses().toList();
    QString invalidAddresses;
    for (QString &addr : addresses) {
        if (!walletModel->validateAddress(addr))
            invalidAddresses += QString("\n%1").arg(addr);
    }

    // Display invalid addresses
    if (!invalidAddresses.isEmpty()) {
        QMessageBox::warning(this->parentWidget(), tr("Issue"),
                             QString("%1:\n%2").arg(tr("Please correct the invalid addresses below"), invalidAddresses));
        return false;
    }

    return true;
}

void CryptoDXSendFunds1::onAddressesChanged() {
    // First add any new addresses (do not overwrite existing)
    auto addresses = addressTi->getAddresses();
    QHash<QString, CryptoDXTransaction> hash;
    for (const QString &addr : addresses) {
        CryptoDXTransaction r(addr);
        hash[addr] = r;
        model->addRecipient(r);
    }
    // Remove any unspecified addresses
    auto txlist = model->recipients.toList();
    for (const CryptoDXTransaction &r : txlist) {
        if (!hash.contains(r.address))
            model->removeRecipient(r);
    }
}
