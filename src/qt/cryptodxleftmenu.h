// Copyright (c) 2018 The CryptoDX Developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#ifndef BLOCKNETLEFTMENU_H
#define BLOCKNETLEFTMENU_H

#include "cryptodxvars.h"
#include "cryptodxiconlabel.h"

#include "amount.h"

#include <QWidget>
#include <QFrame>
#include <QLabel>

class CryptoDXLeftMenu : public QFrame
{
    Q_OBJECT

public:
    explicit CryptoDXLeftMenu(QFrame *parent = nullptr);
    void setBalance(CAmount balance, int unit);
    void selectMenu(CryptoDXPage menuType);

signals:
    void menuChanged(CryptoDXPage menuType);

public slots:

private slots:
    void onMenuSelected(int menuType, bool selected);

private:
    QVBoxLayout *layout;
    QLabel *logo;
    QLabel *balanceLbl;
    QLabel *balanceAmountLbl;

    QButtonGroup *group;
    CryptoDXIconLabel *dashboard;
    CryptoDXIconLabel *addressBook;
    CryptoDXIconLabel *sendFunds;
    CryptoDXIconLabel *requestFunds;
    CryptoDXIconLabel *transactionHistory;
    CryptoDXIconLabel *snodes;
    CryptoDXIconLabel *proposals;
    CryptoDXIconLabel *announcements;
    CryptoDXIconLabel *settings;
    CryptoDXIconLabel *tools;

    QLabel *versionLbl;
};

#endif // BLOCKNETLEFTMENU_H
