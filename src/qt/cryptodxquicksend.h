// Copyright (c) 2018 The CryptoDX Developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#ifndef BLOCKNETQUICKSEND_H
#define BLOCKNETQUICKSEND_H

#include "cryptodxformbtn.h"
#include "cryptodxlineedit.h"
#include "cryptodxsendfundsutil.h"

#include "walletmodel.h"

#include <QFrame>
#include <QLabel>
#include <QVBoxLayout>
#include <QList>
#include <QShowEvent>
#include <QHideEvent>

class CryptoDXQuickSend : public QFrame
{
    Q_OBJECT
public:
    explicit CryptoDXQuickSend(WalletModel *w, QFrame *parent = nullptr);
    bool validated();

signals:
    void dashboard();
    void submit();

public slots:
    void onSubmit();
    void onCancel() { emit dashboard(); }

protected:
    void keyPressEvent(QKeyEvent *event) override;
    void showEvent(QShowEvent *event) override;
    void hideEvent(QHideEvent *event) override;

private slots:
    void onAmountChanged(const QString &text = QString());
    void onDisplayUnit(int);
    void onEncryptionStatus(int encStatus);

private:
    WalletModel *walletModel;
    int displayUnit;
    CAmount lastAmount{0};
    CAmount totalAmount{0};
    CAmount txAmount{0};
    CAmount txFees{0};

    QVBoxLayout *layout;
    QLabel *titleLbl;
    CryptoDXLineEdit *addressTi;
    CryptoDXLineEdit *amountTi;
    QLabel *feeValueLbl;
    QLabel *totalValueLbl;
    QLabel *warningLbl;
    CryptoDXFormBtn *cancelBtn;
    CryptoDXFormBtn *confirmBtn;

    WalletModel::SendCoinsReturn processFunds(bool submitFunds = false);
};

#endif // BLOCKNETQUICKSEND_H