// Copyright (c) 2018 The CryptoDX Developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#ifndef BLOCKNETFORMBTN_H
#define BLOCKNETFORMBTN_H

#include <QPushButton>

class CryptoDXFormBtn : public QPushButton
{
    Q_OBJECT
public:
    explicit CryptoDXFormBtn(QPushButton *parent = nullptr);

signals:

public slots:
};

#endif // BLOCKNETFORMBTN_H