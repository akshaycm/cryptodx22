Sample configuration files for:

SystemD: cryptodxd.service
Upstart: cryptodxd.conf
OpenRC:  cryptodxd.openrc
         cryptodxd.openrcconf
CentOS:  cryptodxd.init

have been made available to assist packagers in creating node packages here.

See doc/init.md for more information.
